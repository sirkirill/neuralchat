package main

import (
	"context"
	"gopkg.in/boj/redistore.v1"
	"log"
	"net/http"
	"time"
)

type AuthenticationMiddleware struct {
	Store *redistore.RediStore
}

func (a *AuthenticationMiddleware) AuthMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		session, err := a.Store.Get(r, "_sid")
		if err != nil {
			log.Println(err)
			next.ServeHTTP(w, r)
		} else {
			sValues := SessionValues{}
			var ok bool
			if sValues, ok = session.Values["param"].(SessionValues); !ok {

			}
			ctx := r.Context()
			ctx = context.WithValue(ctx, "session", sValues)
			next.ServeHTTP(w, r.WithContext(ctx))
		}
	})
}

func PanicMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		log.Println("panicMiddleware", r.URL.Path)
		defer func() {
			if err := recover(); err != nil {
				log.Println("recovered", err)
				http.Error(w, "Internal server error", http.StatusInternalServerError)
			}
		}()
		next.ServeHTTP(w, r)
	})
}

func LogMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		timeReq := time.Now()
		defer func(start time.Time) {
			log.Println(time.Since(start))
		}(timeReq)
		next.ServeHTTP(w, r)
	})
}
