package main

import (
	"bufio"
	"encoding/json"
	"github.com/gobwas/ws"
	"log"
	"net"
)

type Packet struct {
	Msg []Message `json:"msg"`
}

type Message struct {
	Author string `json:"author"`
	Body   string `json:"body"`
}

type Channel struct {
	conn        net.Conn
	send        chan Packet
	noWriterYet bool
	noReaderYet bool
	pool        *Pool
}

func NewChannel(conn net.Conn, pool *Pool) *Channel {
	c := &Channel{
		conn:        conn,
		send:        make(chan Packet, 128),
		noWriterYet: true,
		noReaderYet: true,
		pool:        pool,
	}
	return c
}

func (ch *Channel) Receive() {
	if ch.noReaderYet {
		ch.noReaderYet = false
		ch.pool.Schedule(ch.reader)
	}
}

func (ch *Channel) Send(p Packet) {
	if ch.noWriterYet {
		ch.noWriterYet = false
		ch.pool.Schedule(ch.writer)
	}
	ch.send <- p
}

func (ch *Channel) reader() {
	buf := bufio.NewReader(ch.conn)

	for {
		pkt, _ := ch.readPacket(buf)
		ch.handle(pkt)
	}
}

func (ch *Channel) writer() {
	buf := bufio.NewWriter(ch.conn)

	for pkt := range ch.send {
		err := writePacket(buf, pkt)
		if err != nil {
			log.Println(err)
			return
		}
		buf.Flush()
	}
}

func writePacket(buf *bufio.Writer, pkt Packet) error {
	j, err := json.Marshal(pkt)
	if err != nil {
		return err
	}
	err = ws.WriteFrame(buf, ws.NewBinaryFrame(j))
	if err != nil {
		return err
	}
	return nil
}

func (ch *Channel) readPacket(buf *bufio.Reader) (Packet, error) {
	frame, err := ws.ReadFrame(buf)
	if err != nil {
		return Packet{}, err
	}

	if frame.Header.Masked {
		ws.Cipher(frame.Payload, frame.Header.Mask, 0)
	}

	frame.Header.Masked = false

	pkt := Packet{Msg: make([]Message, 1)}
	err = json.Unmarshal(frame.Payload, &pkt)

	if err != nil {
		log.Println(err)
		return Packet{}, err
	}
	return pkt, nil
}

func (ch *Channel) handle(pkt Packet) {
	ch.Send(pkt)
}
