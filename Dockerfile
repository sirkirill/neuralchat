FROM golang:alpine AS builder

RUN apk update && apk add git
COPY . $GOPATH/src/mypackage/myapp/
WORKDIR $GOPATH/src/mypackage/myapp/

RUN go get -d -v

RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -a -installsuffix cgo -ldflags="-w -s" -o /go/bin/hello

FROM scratch

COPY --from=builder /go/bin/hello /go/bin/hello
COPY ./static /static

EXPOSE 3000
EXPOSE 2052

ENTRYPOINT ["/go/bin/hello"]