package main

import (
	"crypto/rsa"
	"database/sql"
	"encoding/base64"
	"encoding/binary"
	"encoding/gob"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/gorilla/sessions"
	"gopkg.in/boj/redistore.v1"
	"log"
	"math/big"
	"net/http"
	"sync"
)

type Hdl struct {
	SessionsStore *redistore.RediStore
	Db            *sql.DB
	JwkMap        *map[string]JWKKey
	mutex         sync.Mutex
}

type JWK struct {
	Keys []JWKKey `json:"keys"`
}

type JWKKey struct {
	Alg string `json:"alg"`
	E   string `json:"e"`
	Kid string `json:"kid"`
	Kty string `json:"kty"`
	N   string `json:"n"`
	Use string `json:"use"`
}

type SessionValues struct {
	Username string `json:"username"`
	Email    string `json:"email"`
	Sub      string `json:"sub"`
}

func init() {
	gob.Register(SessionValues{})
}

func GetJWKSJson() *map[string]JWKKey {
	respKeys, err := http.Get("https://cognito-idp.us-east-1.amazonaws.com/us-east-1_QTOO6Oqkp/.well-known/jwks.json")
	if err != nil {
		log.Println(err)
	}
	buf := make([]byte, 2048)
	n, _ := respKeys.Body.Read(buf)
	jwk := JWK{}
	err = json.Unmarshal(buf[:n], &jwk)
	if err != nil {
		log.Println(err)
	}

	jwkMap := make(map[string]JWKKey, 0)
	for _, jwk := range jwk.Keys {
		jwkMap[jwk.Kid] = jwk
	}
	return &jwkMap
}

func (h *Hdl) Handler() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		m := r.URL.Query()
		tokenStr := m["id_token"][0]

		token, err := ValidateJWT(&tokenStr, h.JwkMap, &h.mutex)
		if err != nil {
			log.Println(err)
			http.Error(w, "Try again!", http.StatusForbidden)
			return
		}
		claims := token.Claims.(jwt.MapClaims)
		if claims.VerifyIssuer("https://cognito-idp.us-east-1.amazonaws.com/78npp5b1s0r0ulukpmtupnvc7s", false) {
			log.Println("Not Verify Issuer")
			http.Error(w, "Try again!", http.StatusForbidden)
			return
		}

		session, err := h.SessionsStore.New(r, "_sid")
		if err != nil {
			log.Println("auth SessionStore err: ", err)
			http.Error(w, "Try again!", http.StatusForbidden)
			return
		}

		sValues := SessionValues{
			Username: claims["cognito:username"].(string),
			Email:    claims["email"].(string),
			Sub:      claims["sub"].(string),
		}

		session.Values["param"] = sValues
		session.Options = &sessions.Options{
			MaxAge:   3600,
			Secure:   true,
			HttpOnly: true,
		}

		err = h.SessionsStore.Save(r, w, session)
		if err != nil {
			log.Println(err)
		}
	}
}

func ValidateJWT(tokenStr *string, jwk *map[string]JWKKey, mutex *sync.Mutex) (jwt.Token, error) {
	token := &jwt.Token{}
	token, err := jwt.Parse(*tokenStr, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodRSA); !ok {
			return nil, errors.New(fmt.Sprintf("Unexpected signing method: %v", token.Header["alg"]))
		}
		if kid, ok := token.Header["kid"]; ok {
			if kidStr, ok := kid.(string); ok {
				key := JWKKey{}
				mutex.Lock()
				if *jwk == nil {
					jwk = GetJWKSJson()
				}
				if key, ok = (*jwk)[kidStr]; !ok {
					jwk = GetJWKSJson()
					key = (*jwk)[kidStr]
				}
				mutex.Unlock()
				rsaPublicKey := convertKey(key.E, key.N)
				return rsaPublicKey, nil
			}
		}
		return "", nil
	})
	if err != nil {
		return jwt.Token{}, err
	}

	return *token, nil
}

func convertKey(rawE, rawN string) *rsa.PublicKey {
	decodedE, err := base64.RawURLEncoding.DecodeString(rawE)
	if err != nil {
		panic(err)
	}
	if len(decodedE) < 4 {
		ndata := make([]byte, 4)
		copy(ndata[4-len(decodedE):], decodedE)
		decodedE = ndata
	}
	pubKey := &rsa.PublicKey{
		N: &big.Int{},
		E: int(binary.BigEndian.Uint32(decodedE[:])),
	}
	decodedN, err := base64.RawURLEncoding.DecodeString(rawN)
	if err != nil {
		panic(err)
	}
	pubKey.N.SetBytes(decodedN)
	return pubKey
}
