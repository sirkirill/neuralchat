let socket = new WebSocket("wss://neuralhub.ru:2053");
socket.binaryType = "arraybuffer";

socket.onmessage = function (event) {
    const incomingMessage = event.data;
    const msg = decodeMessage(incomingMessage);
    showMessage(msg.author, msg.body);
};

socket.onclose = function (event) {
    socket = new WebSocket("wss://neuralhub.ru:2053");
};

socket.onerror = function (error) {
    console.log(error);
    socket = new WebSocket("wss://neuralhub.ru:2053");
};

function sendMsg() {
    const m = document.getElementById("msgInput").value;
    if (m === "") {
        return
    }
    const data = {
        msg: [
            {
                body: m,
                author: "bot"
            }
        ]
    };
    const obj = JSON.stringify(data);
    if (socket.readyState) {
        socket.send(obj);
        showMessage("you", m);
    }
}
document.getElementById("button_send_msg").onclick = sendMsg;

function showMessage(author, message) {
    const div_m = document.createElement('div');
    div_m.className = "massage";
    div_m.innerHTML = author + ">\t" + message;
    document.getElementById('messages').appendChild(div_m);
}

function decodeMessage(jsonMsg) {
    const denc = new TextDecoder("utf-8");
    const msg = JSON.parse(denc.decode(jsonMsg));
    return msg.msg[0];
}
