package main

import (
	"crypto/rand"
	"crypto/tls"
	"database/sql"
	"fmt"
	"github.com/gobwas/ws"
	"github.com/gorilla/mux"
	_ "github.com/lib/pq"
	"gopkg.in/boj/redistore.v1"
	"html/template"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"time"
)

const DbHost = "awstestdb.c59iw6ioxhxb.us-east-1.rds.amazonaws.com"
const DbPort = "5432"
const DbUser = "awsuser"
const DbName = "testdb"
const DbTable = "iplogging"

func main() {
	cert, err := tls.LoadX509KeyPair("/run/secrets/neuralhub.ru.pem_12.07.19", "/run/secrets/neuralhub.ru.key_12.07.19")
	if err != nil {
		log.Printf("server: loadkeys: %s", err)
		return
	}

	tlscfg := &tls.Config{
		PreferServerCipherSuites: true,
		CurvePreferences: []tls.CurveID{
			tls.CurveP256,
			tls.X25519,
		},
		MinVersion: tls.VersionTLS12,
		CipherSuites: []uint16{
			tls.TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384,
			tls.TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384,
			tls.TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305,
			tls.TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305,
			tls.TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256,
			tls.TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,
		},
		Certificates: []tls.Certificate{cert},
	}

	ln, err := tls.Listen("tcp", ":2052", tlscfg)
	if err != nil {
		log.Println(err)
		return
	}
	defer ln.Close()

	pool := NewPool(1024)

	go func() {
		for {
			conn, err := ln.Accept()
			if err != nil {
				log.Println(err)
			}
			pool.Schedule(func() {
				log.Println("Accept")
				_, err := ws.Upgrade(conn)
				if err != nil {
					log.Fatal(err)
				}

				ch := NewChannel(conn, pool)

				pool.Schedule(func() {
					ch.Receive()
				})
			})
		}
	}()

	tmpl := template.Must(template.ParseFiles("/static/index.html"))

	authHtmlFile, err := os.Open("/static/auth.html")
	if err != nil {
		log.Println(err)
		return
	}
	authHtml, err := ioutil.ReadAll(authHtmlFile)
	if err != nil {
		log.Println(err)
		return
	}
	authHtmlFile.Close()

	db := Connect()
	defer db.Close()

	keyCookie := make([]byte, 64)
	n, err := rand.Read(keyCookie)
	if err != nil {
		log.Println(err)
		return
	}

	sessionStore, err := redistore.NewRediStore(10, "tcp", "neuralhubsessions.mnb5pa.ng.0001.use1.cache.amazonaws.com:8495", "", keyCookie[:n])
	if err != nil {
		log.Println(err)
		return
	}
	defer sessionStore.Close()

	authHandler := Hdl{
		SessionsStore: sessionStore,
		JwkMap:        GetJWKSJson(),
	}

	authMwr := AuthenticationMiddleware{
		Store: authHandler.SessionsStore,
	}

	router := mux.NewRouter()
	router.HandleFunc("/", indexHandler(tmpl)).Methods("GET")
	router.HandleFunc("/auth", authHandle(&authHtml)).Methods("GET")
	router.HandleFunc("/authhelper", authHandler.Handler()).Methods("GET")
	router.HandleFunc("/.well-known/acme-challenge/dnso3PbxGsA8AUhYbocVbxZzlJySiKjUaFgZml-JS3k", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintln(w, "dnso3PbxGsA8AUhYbocVbxZzlJySiKjUaFgZml-JS3k.GjgXNIkRBW4yN7zajp18Ieot2MIckbwsUCqtMAMepLw")
	})
	router.PathPrefix("/").Handler(http.StripPrefix("/", http.FileServer(http.Dir("/static/"))))

	router.Use(authMwr.AuthMiddleware)
	router.Use(LogMiddleware)
	router.Use(PanicMiddleware)

	srv := &http.Server{
		Addr:         ":3000",
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 5 * time.Second,
		IdleTimeout:  1 * time.Minute,
		Handler:      router,

		TLSConfig: tlscfg,
	}

	if err := srv.ListenAndServeTLS("/run/secrets/neuralhub.ru.pem_12.07.19", "/run/secrets/neuralhub.ru.key_12.07.19"); err != nil {
		log.Println(err)
		return
	}
}

func indexHandler(tmpl *template.Template) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		sValues := SessionValues{}
		var ok bool
		if sValues, ok = ctx.Value("session").(SessionValues); ok {

		}
		err := tmpl.Execute(w, sValues)
		if err != nil {
			log.Println(err)
		}
	}
}

func authHandle(file *[]byte) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Write(*file)
	}
}

func Connect() *sql.DB {
	DbPassword, err := ioutil.ReadFile("/run/secrets/dbpassword")
	if err != nil {
		log.Fatal(err)
	}
	dbinfo := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable", DbHost, DbPort, DbUser, DbPassword, DbName)
	db2, err := sql.Open("postgres", dbinfo)
	if err != nil {
		log.Fatal(err)
	}
	return db2
}
